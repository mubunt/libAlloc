//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libAlloc
// Personalized memory dynamic allocation and release routines  with a view to tracing and statistics
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define START_BOLDRED		"\033[1;31m"
#define START_BOLDYELLOW	"\033[1;33m"
#define START_BOLD 			"\033[1m"
#define STOP_COLOR			"\033[0m"
#define	ERROR_LABEL			"MEMORY ALLOCATION ERROR: "
#define INFO_LABEL			"MEMORY ALLOCATION INFORMATION: "
#define error(fmt, ...)		do { \
								fprintf(stderr, "\n" START_BOLDRED ERROR_LABEL fmt STOP_COLOR "\n\n", __VA_ARGS__); \
							} while (0)
#define info(fmt, ...)		do { \
								fprintf(stderr, "\n" START_BOLDYELLOW INFO_LABEL fmt STOP_COLOR "\n\n", __VA_ARGS__); \
							} while (0)
#define ALLOCATION 			50
//------------------------------------------------------------------------------
// ENUMS
//------------------------------------------------------------------------------
typedef enum { MALLOC, CALLOC, REALLOC, REALLOC_FREE, STRDUP, REALPATH, FREE } alloc_t;
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
struct alloc_s {
	void 		*address;
	size_t		size;
	size_t		count;
	char		file[512];
	int 		line;
	alloc_t 	type;
};
//------------------------------------------------------------------------------
// EXTERNAL FUNCTIONS
//------------------------------------------------------------------------------
extern void	myalloc_exit( int );
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
static int				_allocated_memory = 0;	// Number of allocated memories still to be deleted
static struct alloc_s 	*_memoryblocks = NULL;	// Pointer to array of 'alloc_s" structures'
static size_t			_size_memoryblocks = ALLOCATION;
static size_t			_index_memoryblocks = 0;
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void _record(const char *file, int line, void *address, size_t size, size_t count, alloc_t type) {
	if (_memoryblocks == NULL) {
		_memoryblocks = calloc(_size_memoryblocks, sizeof(struct alloc_s));
		if (_memoryblocks == NULL) {
			error("%s", "No memory space avalaible for allocation table. Abort.");
			myalloc_exit(EXIT_FAILURE);
		}
		_index_memoryblocks = 0;
	}
	if (_index_memoryblocks == _size_memoryblocks) {
		_size_memoryblocks += ALLOCATION;
		_memoryblocks = realloc(_memoryblocks, _size_memoryblocks * sizeof(struct alloc_s));
		if (_memoryblocks == NULL) {
			error("%s", "No memory space avalaible for re-allocation table. Abort.");
			myalloc_exit(EXIT_FAILURE);
		}
	}
	_memoryblocks[_index_memoryblocks].address = address;
	_memoryblocks[_index_memoryblocks].size = size;
	_memoryblocks[_index_memoryblocks].count = count;
	_memoryblocks[_index_memoryblocks].line = line;
	_memoryblocks[_index_memoryblocks].type = type;
	strcpy(_memoryblocks[_index_memoryblocks].file, file);
	++_index_memoryblocks;
}

static int _compare(void const *a, void const *b) {
	const struct alloc_s *p1 = a;
	const struct alloc_s *p2 = b;
	return (int)((long)p1->address - (long)p2->address);
}
//------------------------------------------------------------------------------
// LIBRARY FUNCTIONS
//------------------------------------------------------------------------------
void *mymalloc( const char *file, int line, size_t size) {
	void *pointer;
	if (NULL == (pointer = malloc(size * sizeof(char)))) {
		error("%s", "(malloc) No more memory space avalaible for allocation. Abort.");
		myalloc_exit(EXIT_FAILURE);
	}
	*(char *)pointer = '\0';
	++_allocated_memory;
	_record(file, line, pointer, size, 1, MALLOC);
	return pointer;
}
//------------------------------------------------------------------------------
void *mycalloc( const char *file, int line, size_t count, size_t size) {
	void *pointer;
	if (NULL == (pointer = calloc(count, size))) {
		error("%s", "(calloc) No more memory space avalaible for allocation. Abort.");
		myalloc_exit(EXIT_FAILURE);
	}
	++_allocated_memory;
	_record(file, line, pointer, size, count, CALLOC);
	return pointer;
}
//-----------------------------------------------------------------------------
void *myrealloc( const char *file, int line, void *pointer, size_t size) {
	void *newpointer;
	if (NULL == (newpointer = realloc(pointer, size))) {
		error("%s", "(realloc) No more memory space avalaible for allocation. Abort.");
		myalloc_exit(EXIT_FAILURE);
	}
	_record(file, line, pointer, 0, 1, REALLOC_FREE);
	_record(file, line, newpointer, size, 1, REALLOC);
	return newpointer;
}
//-----------------------------------------------------------------------------
char *mystrdup( const char *file, int line, const char *string ) {
	void *pointer;
	if (NULL == (pointer = strdup(string))) {
		error("%s", "(strdup) No more memory space avalaible for allocation. Abort.");
		myalloc_exit(EXIT_FAILURE);
	}
	++_allocated_memory;
	_record(file, line, pointer, strlen(string) + 1, 1, STRDUP);
	return pointer;
}
//-----------------------------------------------------------------------------
void myfree( const char *file, int line, void *pointer ) {
	if (pointer != NULL) {
		free(pointer);
		--_allocated_memory;
		_record(file, line, pointer, 0, 0, FREE);
	}
}
//-----------------------------------------------------------------------------
char *myrealpath( const char *file, int line, const char *path ) {
	char *pointer;
	if (NULL == (pointer = realpath(path, NULL))) {
		error("%s", "(realpath) No more memory space avalaible for allocation. Abort.");
		myalloc_exit(EXIT_FAILURE);
	}
	++_allocated_memory;
	_record(file, line, pointer, strlen(pointer) + 1, 1, REALPATH);
	return pointer;
}
//=============================================================================
void myallocreport( short int action ) {
	if (_allocated_memory) {
		if (_allocated_memory > 0)
			info("There are still %d memory blocks allocated and not freed.", _allocated_memory);
		else
			info("There were %d freed memory blocks that were not allocated.", abs(_allocated_memory));
	} else
		info("%s", "All allocated memory blocks have been freed.");

	if (! action) goto END;

	if (_memoryblocks == NULL) {
		info("%s", "There is no allocation information recorded.");
		return;
	}

	qsort(_memoryblocks, _index_memoryblocks, sizeof(struct alloc_s), _compare);

	const char *label_type[] = { "malloc", "calloc", "realloc", "free/realloc", "strdup", "realpath", "free" };

	fprintf(stderr, START_BOLD "Address         Size (char)  Count  Function      Status" STOP_COLOR "\n\n");
	size_t i = 0;
	while (i < _index_memoryblocks) {
		fprintf(stderr, "%p  %-11ld  %-5ld  %-12s",
		        _memoryblocks[i].address,
		        _memoryblocks[i].size,
		        _memoryblocks[i].count,
		        label_type[_memoryblocks[i].type]);
		switch (_memoryblocks[i].type) {
		case MALLOC:
		case CALLOC:
		case REALPATH:
			fprintf(stderr, "  Allocated in %s, line %d", _memoryblocks[i].file, _memoryblocks[i].line);
			break;
		case REALLOC:
			fprintf(stderr, "  Re-allocated in %s, line %d", _memoryblocks[i].file, _memoryblocks[i].line);
			break;
		case REALLOC_FREE:
			fprintf(stderr, "  Freed in %s, line %d", _memoryblocks[i].file, _memoryblocks[i].line);
			break;
		case STRDUP:
			fprintf(stderr, "  Duplicated in %s, line %d", _memoryblocks[i].file, _memoryblocks[i].line);
			break;
		case FREE:
			fprintf(stderr, "  Freed in %s, line %d", _memoryblocks[i].file, _memoryblocks[i].line);
			break;
		}
		if (i != _index_memoryblocks - 1) {
			if (_memoryblocks[i].address == _memoryblocks[i + 1].address &&
			        (_memoryblocks[i + 1].type == FREE ||
			         _memoryblocks[i + 1].type == REALLOC_FREE)) {
				fprintf(stderr, " then freed in %s, line %d", _memoryblocks[i + 1].file, _memoryblocks[i + 1].line);
				++i;
			}
		}
		fprintf(stderr, "\n");
		++i;
	}
	fprintf(stderr, "\n");
END:
//	fflush(stderr);
	free(_memoryblocks);
	_memoryblocks = NULL;
	_allocated_memory = 0;
	return;
}
//-----------------------------------------------------------------------------
