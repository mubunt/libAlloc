//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: libAlloc
// Personalized memory dynamic allocation and release routines  with a view to tracing and statistics
//------------------------------------------------------------------------------
#ifndef MYALLOC_H
#define MYALLOC_H
//------------------------------------------------------------------------------
// MACROS
//----- ------------------------------------------------------
#define malloc( x )			mymalloc( __FILE__, __LINE__, x )
#define calloc( x, y )		mycalloc( __FILE__, __LINE__, x, y )
#define realloc( x, y )		myrealloc( __FILE__, __LINE__, x, y )
#define free( x )			myfree( __FILE__, __LINE__, x )
#define strdup( x )			mystrdup( __FILE__, __LINE__, x )
#define realpath( x )		myrealpath( __FILE__, __LINE__, x )
//------------------------------------------------------------------------------
// FUNCTIONS
//------------------------------------------------------------------------------
extern void		*mymalloc( const char *, int, size_t );
extern void		*mycalloc( const char *, int, size_t, size_t );
extern void		*myrealloc( const char *, int, void *, size_t );
extern void		myfree( const char *, int, void * );
extern char		*mystrdup( const char *, int, const char * );
extern char		*myrealpath( const char *, int, const char * );
extern void		myallocreport( short int );
//------------------------------------------------------------------------------
#endif	// MYALLOC_H
