# RELEASE NOTES: *libAlloc*, Personalized memory dynamic allocation and release routines  with a view to tracing and statistics.

Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 1.0.7**:
  - Updated build system components.

- **Version 1.0.6**:
  - Updated build system.

- **Version 1.0.5**:
  - Removed unused files.

- **Version 1.0.4**:
  - Updated build system component(s)

- **Version 1.0.3**:
  - Renamed executable built as example as *example_xxx* to be compliant with other projects.

- **Version 1.0.2**:
  - Reworked build system to ease global and inter-project updated.
  - Run *astyle* on sources.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 1.0.1**:
  - Standardization of the installation of executables and libraries in $BIN_DIR, $LIB_DIR anetd $INC_DIR defined in the environment.

- **Version 1.0.0**:
  - First version.
