#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "myalloc.h"

#define N	10

void myalloc_exit( int report ) {
	fprintf(stdout, "That's the application exit routine.\n");
	exit(report);
}

int main() {
	char *addr[N];
	int *itab;
	const char *str = "Hello...";

	for (size_t i = 0; i < N; i++)
		addr[i] = malloc( (i + 1) * 100);
	char *p1 = realpath("../.././");
	addr[0] = realloc(addr[0], 500);
	itab = calloc(20, sizeof(int));
	char *p2 = strdup(str);
	for (size_t i = 0; i < N - 2; i++)
		free(addr[i]);
	free(p2);
	free(p1);
	free(itab);
	myallocreport(1);
	return 0;
}
