 # *libAlloc*, Personalized memory dynamic allocation and release routines  with a view to tracing and statistics.

This library provides an easy-to-use way to test applications that dynamically allocate memory. The usual functions allocating or freeing memory areas, (namely **malloc**, **calloc**, **realloc**, **free**, **strdup** and **realpath**)are replaced, during the compilation phase, by others memorizing their action and their location (source file name and line number). At the end, a report is published on *stderr*.
## LICENSE
**libAlloc** is covered by the GNU General Public License (GPL) version 3 and above.

## EXAMPLE
Hereafter a simple program stored in *./tests/test.c*:

``` C
include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "myalloc.h"

#define N	10

void myalloc_exit( int report ) {
	fprintf(stdout, "That's the application exit routine.\n");
	exit(report);
}

int main() {
	char *addr[N];
	int *itab;
	const char *str = "Hello...";

	for (size_t i = 0; i < N; i++)
		addr[i] = malloc( (i + 1) * 100);
	char *p1 = realpath("../.././");
	addr[0] = realloc(addr[0], 500);
	itab = calloc(20, sizeof(int));
	char *p2 = strdup(str);
	for (size_t i = 0; i < N - 2; i++)
		free(addr[i]);
	free(p2);
	free(p1);
	free(itab);
	myallocreport(1);
	return 0;
}
```

Generation is to be done using *./tests/Makefile*. Need to buid *libAlloc* before that:
``` bash
$ cd tests
$ make all MUTE=
Compiling tests.o
gcc -m64 -I../src -O0 -g -W -Wall -Wextra -Wno-unused -Wconversion -Wwrite-strings -Wstack-protector --std=c99 -pedantic -D_GNU_SOURCE -o tests.o -c tests.c
Linking tests
gcc -m64 -O0 -g -o tests tests.o ../linux/liballoc.a
$
```

Execute the example:
``` bash
$ ./tests

MEMORY ALLOCATION INFORMATION: There are still 2 memory blocks allocated and not freed.

Address         Size (char)  Count  Function      Status

0x5561863f92a0  100          1      malloc        Allocated in tests.c, line 20 then freed in tests.c, line 22
0x5561863ffd60  200          1      malloc        Allocated in tests.c, line 20 then freed in tests.c, line 26
0x5561863ffe30  300          1      malloc        Allocated in tests.c, line 20 then freed in tests.c, line 26
0x5561863fff70  400          1      malloc        Allocated in tests.c, line 20 then freed in tests.c, line 26
0x556186400110  500          1      malloc        Allocated in tests.c, line 20 then freed in tests.c, line 26
0x556186400310  600          1      malloc        Allocated in tests.c, line 20 then freed in tests.c, line 26
0x556186400570  700          1      malloc        Allocated in tests.c, line 20 then freed in tests.c, line 26
0x556186400840  800          1      malloc        Allocated in tests.c, line 20 then freed in tests.c, line 26
0x556186400b70  900          1      malloc        Allocated in tests.c, line 20
0x556186400f00  1000         1      malloc        Allocated in tests.c, line 20
0x5561864012f0  30           1      realpath      Allocated in tests.c, line 21 then freed in tests.c, line 28
0x556186402300  500          1      realloc       Re-allocated in tests.c, line 22 then freed in tests.c, line 26
0x556186402500  4            20     calloc        Allocated in tests.c, line 23 then freed in tests.c, line 29
0x556186402560  9            1      strdup        Duplicated in tests.c, line 24 then freed in tests.c, line 27

$
``` 

To use **libAloc**, what you have to do is the following:
- Include the **libAlloc** header file *myalloc.h*.
- Supply a global function *void myalloc_exit( int )* to cleanly exit your application when be a fatal error isdetected by **libAlloc**.
- Call the routine *myallocreport(n)* to display the report (n = 0 for one-line report, n = 1 for full report).
- Set a *-I* option the **libAlloc** source directory in your gcc compilatin command.
- Add the **liballoc.a** library to your link command.
- **That's all. Do not modify your code more than that.**

## STRUCTURE OF THE APPLICATION
This section walks you through **libAlloc**'s structure. Once you understand this structure, you will easily find your way around in **libAlloc**'s code base.

``` bash
$ yaTree
./                     # Application level
├── examples/          # 
│   ├── Makefile       # Makefile
│   └── basic.c        # Example of 'libAlloc' usage
├── src/               # Source directory
│   ├── Makefile       # Makefile
│   ├── myalloc.c      # Source file
│   ├── myalloc.c.orig # 
│   └── myalloc.h      # Header file to be included in user's program
├── COPYING.md         # GNU General Public License markdown file
├── LICENSE.md         # License markdown file
├── Makefile           # Makefile
├── README.md          # ReadMe markdown file
├── RELEASENOTES.md    # Release Notes markdown file
└── VERSION            # Version identification text file

2 directories, 12 files
$ 
```
## HOW TO BUILD THIS APPLICATION
```Shell
$ cd libAlloc
$ make clean all
```

## HOW TO INSTALL AND USE THIS APPLICATION
```Shell
$ cd libAlloc
$ make release
    # Library generated with -O2 option and the header file, are respectively installed in $LIB_DIR and $INC_DIR directories (defined at environment level).
```

## SOFTWARE REQUIREMENTS
- For usage and development, nothing particular...
- Developped and tested on XUBUNTU 19.04, GCC v8.3.0

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***